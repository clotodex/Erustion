// The `vulkano` crate is the main crate that you must use to use Vulkan.
#[macro_use]
extern crate vulkano;
// The `vulkano_shader_derive` crate allows us to use the `VulkanoShader` custom derive that we use
// in this example.
#[macro_use]
extern crate vulkano_shader_derive;
// However the Vulkan library doesn't provide any functionality to create and handle windows, as
// this would be out of scope. In order to open a window, we are going to use the `winit` crate.
extern crate winit;
// The `vulkano_win` crate is the link between `vulkano` and `winit`. Vulkano doesn't know about
// winit, and winit doesn't know about vulkano, so import a crate that will provide a link between
// the two.
extern crate vulkano_win;

use vulkano_win::VkSurfaceBuild;

use vulkano::buffer::BufferUsage;
use vulkano::buffer::CpuAccessibleBuffer;
use vulkano::command_buffer::AutoCommandBufferBuilder;
use vulkano::command_buffer::DynamicState;
use vulkano::device::Device;
use vulkano::framebuffer::Framebuffer;
use vulkano::framebuffer::Subpass;
use vulkano::instance::Instance;
use vulkano::pipeline::GraphicsPipeline;
use vulkano::pipeline::viewport::Viewport;
use vulkano::swapchain;
use vulkano::swapchain::PresentMode;
use vulkano::swapchain::SurfaceTransform;
use vulkano::swapchain::Swapchain;
use vulkano::swapchain::AcquireError;
use vulkano::swapchain::SwapchainCreationError;
use vulkano::sync::now;
use vulkano::sync::GpuFuture;

use std::iter;
use std::sync::Arc;
use std::mem;

fn main() {

    /// Instance
    let instance = {
        let extensions = vulkano_win::required_extensions();
        Instance::new(None, &extensions, None).expect("failed to create Vulkan instance")
    };

    /// Device
    let physical = vulkano::instance::PhysicalDevice::enumerate(&instance)
        .next()  //TODO choose the best
        .expect("no device available");
    println!("Using device: {} (type: {:?})",
             physical.name(),
             physical.ty());

    /// Window
    let mut events_loop = winit::EventsLoop::new();
    //TODO use more options to create window
    let window =
        winit::WindowBuilder::new().build_vk_surface(&events_loop, instance.clone()).unwrap();

    let mut dimensions = {
        let (width, height) = window.window().get_inner_size_pixels().unwrap();
        [width, height]
    };

    /// Queue selection
    let queue = physical.queue_families()
        .find(|&q| {
            // We take the first queue that supports drawing to our window.
            q.supports_graphics() && window.surface().is_supported(q).unwrap_or(false)
        })
        .expect("couldn't find a graphical queue family");

    let (device, mut queues) = {
        let device_ext = vulkano::device::DeviceExtensions {
            khr_swapchain: true,
            ..vulkano::device::DeviceExtensions::none()
        };

        Device::new(physical,
                    physical.supported_features(),
                    &device_ext,
                    [(queue, 0.5)].iter().cloned())
            .expect("failed to create device")
    };

    // TODO optimize
    let queue = queues.next().unwrap();

    /// swapchain creation
    let (mut swapchain, mut images) = {
        let caps = window.surface()
            .capabilities(physical)
            .expect("failed to get surface capabilities");
        // let dimensions = caps.current_extent.unwrap_or([width, height]);
        let alpha = caps.supported_composite_alpha.iter().next().unwrap();
        //TODO optimize
        let format = caps.supported_formats[0].0;
        //TODO optimize
        Swapchain::new(device.clone(),
                       window.surface().clone(),
                       caps.min_image_count,
                       format,
                       dimensions,
                       1,
                       caps.supported_usage_flags,
                       &queue,
                       SurfaceTransform::Identity,
                       alpha,
                       PresentMode::Fifo, //TODO choose mailbox if available -> non blocking
                       true,
                       None)
            .expect("failed to create swapchain")
    };

    /// VertexBuffer
    let vertex_buffer = {
        #[derive(Debug, Clone)]
        struct Vertex {
            position: [f32; 2],
        }
        impl_vertex!(Vertex, position);

        //We supply data from here (=CPU) and need to modify it by shaders (=GPU) => other buffer
        //types don't fit for this task
        CpuAccessibleBuffer::from_iter(device.clone(),
                                       BufferUsage::all(),
                                       [Vertex { position: [-0.5, -0.25] },
                                        Vertex { position: [0.0, 0.5] },
                                        Vertex { position: [0.25, -0.1] }]
                                           .iter()
                                           .cloned())
            .expect("failed to create buffer")
    };

    /// Shader
    mod vs {
        #[derive(VulkanoShader)]
        #[ty = "vertex"]
        #[src = "
            #version 450

            layout(location = 0) in vec2 position;

            void main() {
                gl_Position = vec4(position, 0.0, 1.0);
            }
        "]
        struct Dummy;
    }

    mod fs {
        #[derive(VulkanoShader)]
        #[ty = "fragment"]
        #[src = "
            #version 450

            layout(location = 0) out vec4 f_color;

            void main() {
                f_color = vec4(1.0, 0.0, 0.0, 1.0);
            }
        "]
        struct Dummy;
    }

    /// Shader loading
    let vs = vs::Shader::load(device.clone()).expect("failed to create vs shader module");
    let fs = fs::Shader::load(device.clone()).expect("failed to create fs shader module");

    /// Rendrpass
    let render_pass = Arc::new(single_pass_renderpass!(device.clone(),
        attachments: {
            // custom name 'color'
            color: {
                load: Clear,
                store: Store,
                format: swapchain.format(),
                // TODO Multisampling
                samples: 1,
            }
        },
        pass: {
            color: [color],
            depth_stencil: {}
        }
    ).unwrap());

    /// Pipeline layout
    let pipeline = Arc::new(GraphicsPipeline::start()
        .vertex_input_single_buffer()
        .vertex_shader(vs.main_entry_point(), ())
        .triangle_list()
        .viewports_dynamic_scissors_irrelevant(1)
        .fragment_shader(fs.main_entry_point(), ())
        .render_pass(Subpass::from(render_pass.clone(), 0).unwrap())
        .build(device.clone())
        .unwrap());

    /// Framebuffers
    let mut framebuffers: Option<Vec<Arc<vulkano::framebuffer::Framebuffer<_, _>>>> = None;

    //###################################### GAME LOOP ########################################

    println!("Initialized! - Running gameloop now..");

    let mut recreate_swapchain = false;
    // prevent frame from destructing
    let mut previous_frame_end = Box::new(now(device.clone())) as Box<GpuFuture>;

    /// main loop
    loop {
        // poll fences
        previous_frame_end.cleanup_finished();

        // Recreate swapchain
        if recreate_swapchain {
            dimensions = {
                let (new_width, new_height) = window.window().get_inner_size_pixels().unwrap();
                [new_width, new_height]
            };
            let (new_swapchain, new_images) = match swapchain.recreate_with_dimension(dimensions) {
                Ok(r) => r,
                // already invalid again O.o restart :D
                Err(SwapchainCreationError::UnsupportedDimensions) => {
                    continue;
                }
                Err(err) => panic!("{:?}", err),
            };

            mem::replace(&mut swapchain, new_swapchain);
            mem::replace(&mut images, new_images);

            framebuffers = None;
            recreate_swapchain = false;
        }

        // Also recreate framebuffers
        if framebuffers.is_none() {
            let new_framebuffers = Some(images.iter()
                .map(|image| {
                    Arc::new(Framebuffer::start(render_pass.clone())
                        .add(image.clone())
                        .unwrap()
                        .build()
                        .unwrap())
                })
                .collect::<Vec<_>>());
            mem::replace(&mut framebuffers, new_framebuffers);
        }

        /// Acquire an image
        let (image_num, acquire_future) = match swapchain::acquire_next_image(swapchain.clone(), None) {
            Ok(r) => r,
            Err(AcquireError::OutOfDate) => {
                recreate_swapchain = true;
                continue;
            }
            Err(err) => panic!("{:?}", err),
        };

        //TODO should be done only on a recreation - but then without one time submit?
        /// Build commandbuffer
        let command_buffer = AutoCommandBufferBuilder::primary_one_time_submit(device.clone(), queue.family()).unwrap()
            //clear color
            // init attachments
            .begin_render_pass(framebuffers.as_ref().unwrap()[image_num].clone(), false, vec![[0.0, 0.0, 1.0, 1.0].into()])
            .unwrap()
            // draw start
            .draw(pipeline.clone(),
                //TODO why can't i store this outside of the loop?
                  DynamicState {
                      line_width: None,
                      // TODO: Find a way to do this without having to dynamically allocate a Vec every frame.
                      viewports: Some(vec![Viewport {
                          origin: [0.0, 0.0],
                          dimensions: [dimensions[0] as f32, dimensions[1] as f32],
                          depth_range: 0.0 .. 1.0,
                      }]),
                      scissors: None,
                  },
                  vertex_buffer.clone(), (), ())
            .unwrap()
            // draw end
            .end_render_pass()
            .unwrap()

            // build commandbuffer
            .build().unwrap();

        /// execute command buffer
        let future = previous_frame_end.join(acquire_future)
            .then_execute(queue.clone(), command_buffer).unwrap()
            // queue present command
            .then_swapchain_present(queue.clone(), swapchain.clone(), image_num)
            .then_signal_fence_and_flush().unwrap();
        previous_frame_end = Box::new(future) as Box<_>;

        /// Window closing
        let mut done = false;
        events_loop.poll_events(|ev| {
            match ev {
                winit::Event::WindowEvent { event: winit::WindowEvent::Closed, .. } => done = true,
                winit::Event::WindowEvent { event: winit::WindowEvent::Resized(_, _), .. } => {
                    recreate_swapchain = true
                }
                _ => (),
            }
        });
        if done {
            println!("Thank you for choosing Erustion :P Bye bye..");
            return;
        }
    }
}
